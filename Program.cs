﻿
using Lab4;

StringParser stringParser = new StringParser();

while(true)
{
    Console.WriteLine("1. Ввести данные\n2. Вывести все телефоны\n3. Вывести все даты\n4. Вывести все числа");
    int menuNumber = 0;
    try { menuNumber = Int32.Parse(Console.ReadLine()); } catch { }
    switch (menuNumber)
    {
        case 1:
            Console.WriteLine(stringParser.Parse(Console.ReadLine()));
            break;
        case 2:
            PhoneNumber.WriteAllPhoneNumbers();
            break;
        case 3:
            Dates.WriteAllDates();
            break;
        case 4:
            Numbers.WriteAllNumbers();
            break;
    }
}