﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    public class StringParser
    {
        
        public string Parse(string str)
        {

            //Phone number2
            if(str.Contains('+'))
            {
                return PhoneNumber.ParsePhoneNumber(str);
            }
            //Date
            if(str.Contains('/'))
            {
                return Dates.ParseDate(str);
            }
            //Number
            return Numbers.ParseNumber(str);
        }
    }
}
