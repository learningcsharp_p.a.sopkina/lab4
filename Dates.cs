﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    public static class Dates
    {
        public static List<DateOnly> AllDates { get; private set; }

        static Dates()
        {
            AllDates = new List<DateOnly>();
        }
        public static void WriteAllDates()
        {
            Console.WriteLine("Все даты:");
            foreach (var p in AllDates)
            {
                Console.WriteLine(p.ToString("dd/MM/yyyy"));
            }
        }
        public static string ParseDate(string str)
        {
            try
            {
                AllDates.Add(DateOnly.ParseExact(str, "dd/MM/yyyy"));
                return $"Вы ввели дату {str}";
            }
            catch { return "Вы ввели некоректную дату!"; }
            
        }

    }
}
