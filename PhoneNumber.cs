﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    public class PhoneNumber
    {
        public static List<PhoneNumber> AllPhonenumber { get; private set; }

        public CountryCode Country { get; private set; }
        public long Number { get; private set; }

        static PhoneNumber()
        {
            AllPhonenumber = new List<PhoneNumber>();
        }
        PhoneNumber(long Number, CountryCode Country)
        {
            this.Number = Number;
            this.Country = Country;
        }
        public override string ToString()
        {
            string result = "";

            string number = Number.ToString();

            result += $"+{(int)Country}({number[0]}{number[1]}{number[2]}){number[3]}{number[4]}{number[5]}-{number[6]}{number[7]}-{number[8]}{number[9]}";

            return result;
        }
        public static void WriteAllPhoneNumbers()
        {
            Console.WriteLine("Все номера:");
            foreach (var p in AllPhonenumber)
            {
                Console.WriteLine(p);
            }
        }
        public static string ParsePhoneNumber(string str)
        {
            if (str.Length < 11)
            {
                return "Введен неправильный номер";
            }
            string number = str[(str.Length - 10)..];
            string contryCode = str[1..(str.Length - 10)];
            try 
            {
                PhoneNumber a = new PhoneNumber(Int64.Parse(number), (CountryCode)Int32.Parse(contryCode));
                AllPhonenumber.Add(a);
                return $"Вы ввели номер: {a}";
            }
            catch { return "Номер введен не правильно!"; }
        }
    }
}
