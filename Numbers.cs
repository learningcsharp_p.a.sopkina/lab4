﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    public static class Numbers
    {
        public static List<int> AllInt { get; private set; }
        public static List<double> AllDouble { get; private set; }

        static Numbers()
        {
            AllInt = new List<int>();
            AllDouble = new List<double>();
        }
        public static void WriteAllNumbers()
        {
            Console.WriteLine("Все целые числа:");
            foreach (var p in AllInt)
            {
                Console.WriteLine(p);
            }
            Console.WriteLine("Все дробные числа:");
            foreach (var p in AllDouble)
            {
                Console.WriteLine($"{p:F2}");
            }
        }
        public static string ParseNumber(string str)
        {
            if (str.Contains(',')) str = str.Replace(',', '.');
            if(str.Contains('.'))
            {
                try
                {
                    double a = double.Parse(str, CultureInfo.InvariantCulture);
                    AllDouble.Add(a);
                    return $"Число {a:F2} добавиленно.";
                }
                catch { return "Вы ввели некоректные символы!"; }
            }
            try
            {
                AllInt.Add(Int32.Parse(str));
                return $"Целое число {Int32.Parse(str)} добавиленно.";
            }
            catch { return "Вы ввели некоректные символы!"; }
        }
    }
}
